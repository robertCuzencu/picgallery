<?php

namespace AppBundle\Command;
/**
 * Created by PhpStorm.
 * User: Robert Cuzencu
 * Date: 04.10.2016
 * Time: 11:18
 */
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use AppBundle\Entity\User;

class CreateUserCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:create-users')
            ->setDescription('Creates new users.')
            ->setHelp("This command allows you to create users...")
            ->addArgument('username', InputArgument::REQUIRED, 'The username of the user.')
            ->addArgument('email', InputArgument::REQUIRED, 'The email of the user.')
            ->addArgument('password', InputArgument::REQUIRED, 'The password of the user.')
            ->addArgument('role', InputArgument::REQUIRED, 'The role of the user.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Hey! Creating new user:)');

        $user = new User();
        $user->setUsername($input->getArgument('username'));
        $user->setEmail($input->getArgument('email'));

        $password = $this->getContainer()->get('security.password_encoder')
            ->encodePassword($user, $input->getArgument('password'));
        $user->setPassword($password);


        $user->setRoles($input->getArgument('role'));

        $em = $this->getContainer()->get('doctrine')->getEntityManager('default');
        $em->persist($user);
        $em->flush();

        $output->writeln('Username created: '.$input->getArgument('username').' with the email: '.$input->getArgument('email'));
    }
}