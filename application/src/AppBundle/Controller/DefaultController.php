<?php

namespace AppBundle\Controller;

use AppBundle\Form\FotoUploadForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Fotos;
use Symfony\Component\Templating\Helper\AssetsHelper;

class DefaultController extends Controller
{

    public function showPicsAction(Request $request)
    {
        $fotosNo = count($this->getDoctrine()->getRepository('AppBundle:Fotos')->loadAllFotos());
        return $this->render('PicGallery/ShowPics.html.twig', array('fotosNo' => $fotosNo,
        ));
    }

    public function loginAction(Request $request)
    {
        $authenticationUtils = $this->get('security.authentication_utils');

        $error = $authenticationUtils->getLastAuthenticationError();

        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', array(
            'last_username' => $lastUsername,
            'error'         => $error,
        ));
    }

    public function uploadAction(Request $request)
    {
        if (!($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN') or ($this->get('security.authorization_checker')->isGranted('ROLE_USER')))) {
            $this->addFlash('error', 'You musst have certain rights in order to perform this action.');
            return $this->redirectToRoute('login');
        }
        $foto = new Fotos();
        $form = $this->createForm(FotoUploadForm::class, $foto);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // $file stores the uploaded PDF file
            /** @var Symfony\Component\HttpFoundation\File\UploadedFile $file */
            $file = $foto->getFileName();
            $extens = $file->guessExtension();

            if(!($extens == "jpg" || $extens == "png" || $extens == "gif")){
                $this->addFlash('error', 'You are allowed to upload photos only.');
                return $this->redirectToRoute('upload');
            }

            // Generate a unique name for the file before saving it
            $fileName = md5(uniqid()).'.'.$extens;

            // Move the file to the directory where fotos are stored
            $fotosDirectory = $this->getParameter('fotosDirectory');

            $file->move(
                $fotosDirectory,
                $fileName
            );
            // Update the 'fileName' property to store the PDF file name
            // instead of its contents
            $foto->setFileName($fileName);

            //Create smaller Photos
            $smallFotosDirectory = $this->getParameter('smallFotosDirectory');
            $this->get('helper.imageresizer')->resizeImage($fotosDirectory. "/".$fileName, $smallFotosDirectory , 0, 150);
            // Generate a unique name for the small foto before saving it
            $smallFotoFileName = 'small'.$fileName;

            $foto->setSmallFotoFileName($smallFotoFileName);

            $em = $this->getDoctrine()->getManager();
            $em->persist($foto);
            $em->flush();

            $this->addFlash('success', 'Successfully saved informations');
            return $this->redirectToRoute('upload');
        }

        return $this->render('PicGallery/FotoUpload.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    public function retrieveFotosAction(Request $request)
    {
        $aux = $request->query->get('aux');

        $aux2 = array();

        $fotos = $this->getDoctrine()->getRepository('AppBundle:Fotos')->loadFotosByFileName($aux);

        foreach($fotos as $i) {
            $aux2[] = [
                "lowsrc" => '/uploads/smallFotos/'.$i->getSmallFotoFileName(),
                "fullsrc" => '/uploads/fotos/'.$i->getFileName(),
                "description" => $i->getName(),
                "category" => "all",

            ];
        }
        return new JsonResponse($aux2);
    }
}
