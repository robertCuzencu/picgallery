#!/bin/sh

# If you would like to do some extra provisioning you may
# add any commands you wish to this file and they will
# be run after the Homestead machine is provisioned.

echo "10.64.93.169 obs.ws.web30ag.net kitt.web30ag.net kitty.web30ag.net kitty2.web30ag.net forecast.ws.wetter.com astronomy.ws.wetter.com observation.ws.wetter.com forecast.wettercom.net astronomy.wettercom.net observation.wettercom.net" >> /etc/hosts
echo "10.64.93.155 cms.wetterstage.com" >> /etc/hosts

sed -i 's/cgi.fix_pathinfo=0/cgi.fix_pathinfo=1/g' /etc/php/7.0/fpm/php.ini;

service nginx restart;
service php7.0-fpm restart;